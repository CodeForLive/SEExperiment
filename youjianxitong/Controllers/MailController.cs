﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace youjianxitong.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult List2()
        {
            return View("List");
        }
        public JsonResult GetList()
        {
            BLL.T_Base_Mail bllUser = new BLL.T_Base_Mail();
            List<Model.T_Base_Sum> lst = bllUser.GetAllList();
            return Json(lst);
        }

        public JsonResult GetListByPage(int pageSize, int pageNumber, string sortName, string sortOrder, string mailId, string recName, string recPhone, string recAddress, string postAddress, string postName)
        {
            BLL.T_Base_Mail bllUser = new BLL.T_Base_Mail();
            List<Model.T_Base_Message> lst = bllUser.GetListByPage(pageSize, pageNumber, sortName, sortOrder, mailId, recName, recPhone, recAddress, postAddress, postName);
            int totalcount = bllUser.GetCount(pageSize, pageNumber, sortName, sortOrder, mailId, recName, recPhone, recAddress, postAddress, postName);
            var result = new { rows = lst, total = totalcount };
            return Json(result);
        }

        public JsonResult AddSave(Model.T_Base_Mail model, Model.T_Base_Receiver model2)
        {

            BLL.T_Base_Mail userBll = new BLL.T_Base_Mail();
            int result = userBll.Add(model, model2);
            if (result >= 1)
            {
                return Json(new { code = 1, message = "插入成功" });
            }
            else
            {
                return Json(new { code = 0, message = "插入失败" });
            }


        }

        public ActionResult Edit(int? id)
        {
            Model.T_Base_Sum user = new Model.T_Base_Sum();
            BLL.T_Base_Mail bllUser = new BLL.T_Base_Mail();
            user = bllUser.GetModel(id);
            ViewBag.user = user;
            return View();
        }

        public JsonResult EditSave(Model.T_Base_Sum user)
        {
            BLL.T_Base_Mail bllUser = new BLL.T_Base_Mail();
            int result = bllUser.Update(user);
            if (result > 0)
            {
                return Json(new { code = 1, message = "删除成功" });
            }
            else
                return Json(new { code = 0, message = "删除失败" });

        }

        public JsonResult Delete(int? id)
        {
            BLL.T_Base_Mail bllUser = new BLL.T_Base_Mail();
            int result = bllUser.Delete(id);
            if (result > 0)
            {
                return Json(new { code = 1, message = "删除成功" });
            }
            else
                return Json(new { code = 0, message = "删除失败" });

        }
    }
}