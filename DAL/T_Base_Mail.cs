﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DAL
{
    public class T_Base_Mail
    {
        public List<Model.T_Base_Sum> GetAllList()
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
            co.Open();

            SqlCommand cm = new SqlCommand();
            cm.CommandText = "select * from T_Base_Message";
            cm.Connection = co;

            List<Model.T_Base_Sum> lst = new List<Model.T_Base_Sum>();
            SqlDataReader dr = cm.ExecuteReader();
            while (dr.Read())
            {

                Model.T_Base_Message mess = new Model.T_Base_Message();
                Model.T_Base_Sum sum = new Model.T_Base_Sum();

                sum.rec_id = Convert.ToString(dr["rec_id"]);
                sum.id = Convert.ToString(dr["mail_id"]);
                sum.mid = Convert.ToInt32(dr["mid"]);
                SqlConnection co1 = new SqlConnection();
                co1.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
                co1.Open();
                SqlCommand cm1 = new SqlCommand();
                cm1.CommandText = "select * from t_bash_mail where id ='" + Convert.ToString(dr["mail_id"]) + "'";
                cm1.Connection = co1;
                SqlDataReader dr1 = cm1.ExecuteReader();
                while (dr1.Read())
                {
                    sum.type = Convert.ToString(dr1["type"]);
                    sum.rec_addr = Convert.ToString(dr1["rec_addr"]);
                    sum.input_time = Convert.ToDateTime(dr1["input_time"]);
                    sum.post_addr = Convert.ToString(dr1["post_addr"]);
                    sum.sign_for = Convert.ToInt32(dr1["sign_for"]);
                    sum.post_name = Convert.ToString(dr1["post_name"]);
                    if (!Convert.IsDBNull(dr1["overtime"]))
                    {
                        sum.overtime = Convert.ToInt32(dr1["overtime"]);
                    }

                    if (!Convert.IsDBNull(dr1["overdue"]))
                    {
                        sum.overdue = Convert.ToDateTime(dr1["overdue"]);
                    }

                    if (!Convert.IsDBNull(dr1["rec_time"]))
                    {
                        sum.rec_time = Convert.ToDateTime(dr1["rec_time"]);
                    }
                }
                dr1.Close();
                co1.Close();

                SqlConnection co2 = new SqlConnection();
                co2.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
                co2.Open();
                SqlCommand cm2 = new SqlCommand();
                cm2.CommandText = "select * from t_base_receiver where rec_id ='" + Convert.ToString(dr["rec_id"]) + "'";
                cm2.Connection = co2;
                SqlDataReader dr2 = cm2.ExecuteReader();
                while (dr2.Read())
                {
                    sum.name = Convert.ToString(dr2["name"]);
                    sum.phone_number = Convert.ToString(dr2["phone_number"]);
                }
                dr2.Close();
                co2.Close();
                lst.Add(sum);
            }
            dr.Close();
            co.Close();
            return lst;

        }

        public int GetCount(int pageSize, int pageNumber, string sortName, string sortOrder, string mailId, string recName, string recPhone, string recAddress, string postAddress, string postName)
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = ConfigurationManager.ConnectionStrings["sqlconnection"].ToString();
            co.Open();
            SqlCommand cm = new SqlCommand();
            cm.Connection = co;
            string where = "id LIKE '%" + mailId + "%' AND name LIKE N'%" + recName + "%' AND phone_number LIKE '%" + recPhone + "%' AND rec_addr LIKE N'%" + recAddress + "%' AND post_name LIKE N'%" + postName + "%' AND post_addr LIKE N'%" + postAddress + "%'";
            cm.CommandText = "SELECT  count(*) FROM v_mail_info WHERE  " + where;
            cm.Connection = co;

            Object count = cm.ExecuteScalar();
            co.Close();
            return (Int32)count;

        }

        public List<Model.T_Base_Message> GetListByPage(int pageSize, int pageNumber, string sortName, string sortOrder,string mailId, string recName, string recPhone, string recAddress, string postAddress, string postName)
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = ConfigurationManager.ConnectionStrings["sqlconnection"].ToString();
            co.Open();
            SqlCommand cm = new SqlCommand();
            cm.Connection = co;
            int skipcount = (pageNumber - 1) * pageSize;
            string order = " order by " + sortName + " " + sortOrder + " ";
            string where = "id LIKE '%" + mailId + "%' AND name LIKE N'%" + recName + "%' AND phone_number LIKE '%" + recPhone + "%' AND rec_addr LIKE N'%" + recAddress + "%' AND post_name LIKE N'%" + postName + "%' AND post_addr LIKE N'%" + postAddress + "%'";
            string subtable = "(SELECT TOP " + skipcount + " mid FROM v_mail_info WHERE " + where + " " + order + "  )";
            cm.CommandText = "SELECT TOP " + pageSize + " * FROM v_mail_info WHERE  " + where + " and mid not in " + subtable + " " + order;

            SqlDataReader dr = cm.ExecuteReader();
            List<Model.T_Base_Message> lst = new List<Model.T_Base_Message>();
            while (dr.Read())
            {
                Model.T_Base_Message messageModel = new Model.T_Base_Message();
                Model.T_Base_Mail mailModel = new Model.T_Base_Mail();
                Model.T_Base_Receiver recModel = new Model.T_Base_Receiver();

                messageModel.mid = Convert.ToInt32(dr["mid"]);
                mailModel.id = Convert.ToString(dr["id"]);
                mailModel.input_time = Convert.ToDateTime(dr["input_time"]);
                mailModel.overtime = Convert.ToInt32(dr["overtime"]);
                mailModel.post_addr = Convert.ToString(dr["post_addr"]);
                mailModel.post_name = Convert.ToString(dr["post_name"]);
                mailModel.rec_addr = Convert.ToString(dr["rec_addr"]);
                mailModel.sign_for = Convert.ToInt32(dr["sign_for"]);
                mailModel.type = Convert.ToString(dr["type"]);
                recModel.name = Convert.ToString(dr["name"]);
                recModel.phone_number = Convert.ToString(dr["phone_number"]);

                messageModel.mail = mailModel;
                messageModel.receiver = recModel;

                lst.Add(messageModel);
            }
            dr.Close();
            co.Close();
            return lst;
        }

        public int Add(Model.T_Base_Mail model, Model.T_Base_Receiver model2)
        {
            //把数据存入数据库
            //ado.net插入数据库
            SqlConnection co = new SqlConnection();
            co.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
            co.Open();

            SqlCommand cm = new SqlCommand();

            cm.CommandText = "select count(*) from t_bash_mail";
            cm.Connection = co;

            SqlDataAdapter da = new SqlDataAdapter(cm.CommandText, co);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            int result1 = Convert.ToInt32(dt1.Rows[0][0]);
            DateTime dt = DateTime.Now;
            string a = String.Format("{0:D4}", dt.Month).ToString();
            string str = dt.Year.ToString() + String.Format("{0:D2}", dt.Month).ToString() + String.Format("{0:D2}", dt.Day).ToString() + String.Format("{0:D2}", result1).ToString(); ;
            cm.CommandText = "insert into t_bash_mail (id,type,rec_addr,input_time,post_addr,sign_for,post_name) values (@id,@type,@rec_addr,@input_time,@post_addr,@sign_for,@post_name)";
            cm.Parameters.AddWithValue("@id", str);
            cm.Parameters.AddWithValue("@type", model.type);
            cm.Parameters.AddWithValue("@input_time", dt);
            cm.Parameters.AddWithValue("@post_addr", model.post_addr);
            cm.Parameters.AddWithValue("@sign_for", model.sign_for);
            cm.Parameters.AddWithValue("@post_name", model.post_name);
            cm.Parameters.AddWithValue("@rec_addr", model.rec_addr);
            cm.Connection = co;
            int result = cm.ExecuteNonQuery();



            SqlCommand cm1 = new SqlCommand();
            cm1.CommandText = "insert into t_base_receiver (rec_id,phone_number,age,name) values (@rec_id,@phone_number,@age,@name)";
            cm1.Parameters.AddWithValue("@rec_id", model2.rec_id);
            cm1.Parameters.AddWithValue("@phone_number", model2.phone_number);
            cm1.Parameters.AddWithValue("@age", model2.age);
            cm1.Parameters.AddWithValue("@name", model2.name);
            cm1.Connection = co;
            int result2 = cm1.ExecuteNonQuery();



            SqlCommand cm2 = new SqlCommand();
            cm2.CommandText = "insert into t_base_message (rec_id,mail_id) values (@rec_id,@mail_id)";
            cm2.Parameters.AddWithValue("@rec_id", model2.rec_id);
            cm2.Parameters.AddWithValue("@mail_id", str);

            cm2.Connection = co;
            int result3 = cm2.ExecuteNonQuery();
            co.Close();
            return result2;

        }

        public Model.T_Base_Sum GetModel(int? id)
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
            co.Open();

            SqlCommand cm = new SqlCommand();
            cm.CommandText = "select * from t_base_message where mid=@id";
            cm.Parameters.AddWithValue("@id", id);

            cm.Connection = co;
            SqlDataReader dr = cm.ExecuteReader();
            Model.T_Base_Sum user = new Model.T_Base_Sum();
            user.mid = Convert.ToInt32(id);
            while (dr.Read())
            {
                user.rec_id = Convert.ToString(dr["rec_id"]);
                user.mail_id = Convert.ToString(dr["mail_id"]);

            }
            dr.Close();

            SqlCommand cm1 = new SqlCommand();
            cm1.CommandText = "select * from t_bash_mail where id='" + user.mail_id + "'";
            cm1.Connection = co;
            SqlDataReader dr1 = cm1.ExecuteReader();
            while (dr1.Read())
            {
                user.rec_addr = Convert.ToString(dr1["rec_addr"]);
                user.post_addr = Convert.ToString(dr1["post_addr"]);
                user.post_name = Convert.ToString(dr1["post_name"]);
                user.type = Convert.ToString(dr1["type"]);
            }
            dr1.Close();

            SqlCommand cm2 = new SqlCommand();
            cm2.CommandText = "select * from t_base_receiver where rec_id='" + user.rec_id + "'";
            cm2.Connection = co;
            SqlDataReader dr2 = cm2.ExecuteReader();
            while (dr2.Read())
            {
                user.name = Convert.ToString(dr2["name"]);
                user.phone_number = Convert.ToString(dr2["phone_number"]);

            }
            dr2.Close();
            co.Close();

            return user;
        }

        public int Update(Model.T_Base_Sum user)
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
            co.Open();

            SqlCommand cm = new SqlCommand();
            cm.CommandText = "update t_bash_mail set type=@type,post_addr=@post_addr,post_name=@post_name where id=@mid";
            cm.Parameters.AddWithValue("@type", user.type);
            cm.Parameters.AddWithValue("@post_addr", user.post_addr);
            cm.Parameters.AddWithValue("@post_name", user.post_name);

            cm.Parameters.AddWithValue("@mid", user.mail_id);
            cm.Connection = co;

            int result = cm.ExecuteNonQuery();


            SqlCommand cm1 = new SqlCommand();
            cm1.CommandText = "update t_base_receiver set rec_id=@rec_id,name=@name,phone_number=@phone_number where rec_id=@id1";
            cm1.Parameters.AddWithValue("@id1", user.rec_id);
            cm1.Parameters.AddWithValue("@name", user.name);
            cm1.Parameters.AddWithValue("@phone_number", user.phone_number);
            cm1.Parameters.AddWithValue("@rec_id", user.rec_id);
            cm1.Connection = co;

            int result1 = cm1.ExecuteNonQuery();

            SqlCommand cm2 = new SqlCommand();
            cm2.CommandText = "update t_base_message set rec_id=@rec_id,mail_id=@mail_id where mid=@mid";
            cm2.Parameters.AddWithValue("@rec_id", user.rec_id);
            cm2.Parameters.AddWithValue("@mail_id", user.mail_id);
            cm2.Parameters.AddWithValue("@mid", user.mid);

            cm2.Connection = co;

            int result2 = cm1.ExecuteNonQuery();
            co.Close();
            return result;


        }

        public int Delete(int? id)
        {
            SqlConnection co = new SqlConnection();
            co.ConnectionString = "server=10.132.226.232;database=lkl;uid=sa;pwd=wzu@Jsj123";
            co.Open();

            SqlCommand cm2 = new SqlCommand();
            cm2.CommandText = "select * from t_bash_mail where id='" + id + "'";
            cm2.Connection = co;
            SqlDataReader dr2 = cm2.ExecuteReader();
            DateTime date = new DateTime();
            while (dr2.Read())
            {
                date = Convert.ToDateTime(dr2["input_time"]);

            }
            dr2.Close();

            DateTime dt = DateTime.Now;
            SqlCommand cm = new SqlCommand();
            cm.CommandText = "update t_bash_mail set sign_for=@sign_for,overtime=@overtime,rec_time=@rec_time where id=@mid";
            cm.Parameters.AddWithValue("@mid", id);
            int k = 1;
            cm.Parameters.AddWithValue("@sign_for", k);
            cm.Parameters.AddWithValue("@rec_time", dt);
            TimeSpan st1 = new TimeSpan(date.Ticks);
            TimeSpan st2 = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan ts3 = st1.Subtract(st2).Duration();
            int days = (int)ts3.TotalDays;
            cm.Parameters.AddWithValue("@overtime", days);
            cm.Connection = co;

            int result = cm.ExecuteNonQuery();
            co.Close();
            return 1;
        }
    }
}
