﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class T_Base_Receiver
    {
        public string rec_id { set; get; }
        public string phone_number { set; get; }
        public int age { set; get; }
        public string name { set; get; }
    }
}
