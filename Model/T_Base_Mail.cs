﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class T_Base_Mail
    {
        public string id { set; get; }
        public string type { set; get; }
        public string rec_addr { set; get; }
        public DateTime input_time { set; get; }
        public string post_addr { set; get; }
        public int sign_for { set; get; }
        public string post_name { set; get; }
        public DateTime overdue { set; get; }
        public DateTime rec_time { set; get; }
        public int cont { set; get; }
        public int overtime { set; get; }
    }
}
