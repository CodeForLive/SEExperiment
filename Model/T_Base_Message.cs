﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class T_Base_Message
    {
        public int mid { set; get; }
        public string rec_id { set; get; }
        public string content { set; get; }
        public string mail_id { set; get; }
        public DateTime send_time { set; get; }
        public T_Base_Mail mail{ set; get; }
        public T_Base_Receiver receiver { set; get; }
    }
}
