﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class T_Base_Sum
    {
        public string rec_id { set; get; }
        public string phone_number { set; get; }
        public int age { set; get; }
        public string name { set; get; }
        public string id { set; get; }
        public string type { set; get; }
        public string rec_addr { set; get; }
        public DateTime input_time { set; get; }
        public string post_addr { set; get; }
        public int sign_for { set; get; }
        public string post_name { set; get; }
        public DateTime overdue { set; get; }
        public DateTime rec_time { set; get; }
        public int cont { set; get; }
        public string content { set; get; }
        public string mail_id { set; get; }
        public DateTime send_time { set; get; }
        public int mid { set; get; }
        public int overtime { set; get; }
    }
}
