﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class T_Base_Admin
    {
        public int Add(Model.T_Base_Admin model)
        {
            //记录日志
            DAL.T_Base_Admin userDal = new DAL.T_Base_Admin();
            return userDal.Add(model);
        }
        public int check(string LoginName,string PWD)
        {
            DAL.T_Base_Admin dal = new DAL.T_Base_Admin();
            return dal.check(LoginName, PWD);
        }
        
    }
}
