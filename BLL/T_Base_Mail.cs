﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BLL
{
    public class T_Base_Mail
    {
        public List<Model.T_Base_Sum> GetAllList()
        {
            //记录日志
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.GetAllList();

        }
        public int Add(Model.T_Base_Mail model,Model.T_Base_Receiver model2)
        {
            //记录日志
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.Add(model, model2);

        }
        public Model.T_Base_Sum GetModel(int? id)
        {
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.GetModel(id);
        }

        public List<Model.T_Base_Message> GetListByPage(int pageSize, int pageNumber, string sortName, string sortOrder, string mailId, string recName, string recPhone, string recAddress, string postAddress, string postName)
        {
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.GetListByPage(pageSize, pageNumber, sortName, sortOrder, mailId, recName, recPhone, recAddress, postAddress, postName);
        }

        public int GetCount(int pageSize, int pageNumber, string sortName, string sortOrder, string mailId, string recName, string recPhone, string recAddress, string postAddress, string postName)
        {
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.GetCount(pageSize, pageNumber, sortName, sortOrder, mailId, recName, recPhone, recAddress, postAddress, postName);
        }

        public int Update(Model.T_Base_Sum user)
        {
            
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.Update(user);


        }

        public int Delete(int? id)
        {
            //记录日志
            DAL.T_Base_Mail userDal = new DAL.T_Base_Mail();
            return userDal.Delete(id);

        }
    }
}
